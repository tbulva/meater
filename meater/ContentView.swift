//
//  ContentView.swift
//  meater
//
//  Created by Tomas Bulva on 14/10/2023.
//

import SwiftUI

enum Sheets: Identifiable {
    case login
    
    var id: Int {
        switch self {
            case .login:
                return 1
        }
    }
}

enum WindowSize {
    static let min = CGSize(width: 340, height: 200)
    static let max = CGSize(width: 500, height: 200)
}
    
struct ContentView: View {
    @EnvironmentObject var meaterModel: MeaterModel
    @EnvironmentObject var auth: Auth
    @State var activeSheet: Sheets?
    
    var body: some View {
        NavigationStack {
            List {
                if(!auth.loggedIn) {
                    notLoggedIn()
                }
                else if($meaterModel.devices.isEmpty) {
                    noProbesView()
                } else {
                    ForEach($meaterModel.devices.indices, id: \.self) { index in
                        HStack(alignment: .center) {
                            Spacer()
                            VStack() {
                                Text(cookName(index))
                                    .foregroundColor(Color.white)
                                HStack {
                                    VStack {
                                        Text(Measurement(value: internalTemp(index), unit: UnitTemperature.celsius), formatter: formatter)
                                            .bold()
                                            .foregroundColor(Color.white)
                                            .fixedSize(horizontal: false, vertical: true)
                                            .multilineTextAlignment(.center)
                                            .padding()
                                            .frame(width: 80, height: 80)
                                            .background(Circle().fill(Color.purple).shadow(radius: 3))
                                        Text("Internal")
                                            .foregroundColor(Color.white)
                                    }
                                    
                                    VStack {
                                        Text(Measurement(value: targetTemp(index), unit: UnitTemperature.celsius), formatter: formatter)
                                            .bold()
                                            .foregroundColor(Color.white)
                                            .fixedSize(horizontal: false, vertical: true)
                                            .multilineTextAlignment(.center)
                                            .padding()
                                            .frame(width: 80, height: 80)
                                            .background(Circle().fill(Color.blue).shadow(radius: 3))
                                        Text("Target")
                                            .foregroundColor(Color.white)
                                    }
                                    
                                    VStack {
                                        Text(Measurement(value: ambientTemp(index), unit: UnitTemperature.celsius), formatter: formatter)
                                            .bold()
                                            .foregroundColor(Color.white)
                                            .fixedSize(horizontal: false, vertical: true)
                                            .multilineTextAlignment(.center)
                                            .padding()
                                            .frame(width: 80, height: 80)
                                            .background(Circle().fill(Color.green).shadow(radius: 3))
                                        Text("Ambient")
                                            .foregroundColor(Color.white)
                                    }
                                }
                            }
                            Spacer()
                        }
                        .padding()
                        .onReceive(timer, perform: { _ in
                            Task {
                                if !auth.loggedIn {
                                    activeSheet = .login
                                } else {
                                    let token = auth.getAccessToken()
                                    await meaterModel.fetchDevices(token!)
                                }
                            }
                        })
                    }
                }
            }
            .scrollContentBackground(.hidden)
            .task {
                if !auth.loggedIn {
                    activeSheet = .login
                } else {
                    let token = auth.getAccessToken()
                    await meaterModel.fetchDevices(token!)
                }
            }
            .toolbar {
                ItemsToolbar(refresh: refreshAction, logout: logoutAction, login: loginAction, userIsLoggedin: auth.loggedIn);
            }
        }
        .sheet(item: $activeSheet, content: { sheet in
            switch sheet {
            case .login:
                LoginView()
                    .frame(minWidth: 400, minHeight: 200)
                    .frame(maxWidth: 400, maxHeight: 200)
            }
        })
        .frame(minWidth: WindowSize.min.width, minHeight: WindowSize.min.height)
        .frame(maxWidth: WindowSize.max.width, maxHeight: WindowSize.max.height)
    }
    
    func refreshAction() {
        Task {
            let token = auth.getAccessToken()
            await meaterModel.fetchDevices(token!)
        }
    }
    
    func logoutAction() {
        Task {
            auth.logout()
        }
    }
    
    func loginAction() {
        Task {
            activeSheet = .login
        }
    }
    
    let timer = Timer.publish(every: 30, on: .main, in: .common).autoconnect()
    
    var formatter: MeasurementFormatter {
        let formatter = MeasurementFormatter()
        formatter.unitStyle = .medium
        formatter.numberFormatter.maximumFractionDigits = 1
        formatter.unitOptions = .providedUnit
        return formatter
    }
    
    func internalTemp(_ index: Int) -> Double {
        if let temp = self.meaterModel.devices[index].temperature.internalTemp {
            return Double(temp)
        }
        return Double(0.0)
    }
    
    func targetTemp(_ index: Int) -> Double {
        if let temp = self.meaterModel.devices[index].cook?.temperature?.targetTemp {
            return Double(temp)
        }
        return Double(0.0)
    }
    
    func ambientTemp(_ index: Int) -> Double {
        if let temp = self.meaterModel.devices[index].temperature.ambientTemp {
            return Double(temp)
        }
        return Double(0.0)
    }
    
    func cookName(_ index: Int) -> String {
        if let name = self.meaterModel.devices[index].cook?.name {
            return name
        }
        return "No cook setup yet."
    }
}

struct ItemsToolbar: ToolbarContent {
    let refresh: () -> Void
    let logout: () -> Void
    let login: () -> Void
    let userIsLoggedin: Bool
    var body: some ToolbarContent {
        ToolbarItemGroup() {
            switch userIsLoggedin {
            case true:
                Button {
                    refresh()
                }
                label: {
                    Text("refresh")
                        .padding(.horizontal, 8)
                        .padding(.vertical, 8)
                        .cornerRadius(8)
                }
                
                Button {
                    logout()
                }
                label: {
                    Text("logout")
                        .padding(.horizontal, 8)
                        .padding(.vertical, 8)
                        .cornerRadius(8)
                }
                
            case false:
                Button {
                    login()
                }
                label: {
                    Text("login")
                        .padding(.horizontal, 8)
                        .padding(.vertical, 8)
                        .foregroundColor(Color(nsColor: .controlTextColor))
                        .background(Color(nsColor: .controlAccentColor))
                        .cornerRadius(8)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let service = MeaterService(baseURL: URL(string: "https://public-api.cloud.meater.com")!)
        ContentView().environmentObject(MeaterModel(meaterService: service)).environmentObject(Auth.shared)
    }
}
