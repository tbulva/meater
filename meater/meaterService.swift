//
//  meaterService.swift
//  meater
//
//  Created by Tomas Bulva on 17/10/2023.
//

import Foundation

enum MeaterServiceError: Error {
    case badURL
}

class MeaterService {
    
    let baseURL: URL
    
    init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    func getDevices(_ token: String, completion: @escaping ([Device]?) -> Void) throws {
        guard let url = URL(string: "/v1/devices", relativeTo: baseURL) else {
            throw MeaterServiceError.badURL
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
//        dump(request)
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if let data = data {
//                let urlContent = NSString(data: data, encoding: String.Encoding.ascii.rawValue)
//                print(urlContent)
//
                do {
                    let response = try JSONDecoder().decode(MeaterData.self, from: data)
//                    print(response as Any)
                    completion(response.data.devices)
                } catch DecodingError.dataCorrupted(let context) {
                    print(context)
                } catch DecodingError.keyNotFound(let key, let context) {
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch DecodingError.valueNotFound(let value, let context) {
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    print("error: ", error)
                }
            }
        }
        task.resume()
    }
    
    func getToken(_ loginCredentials: LoginCredentials, completion: @escaping (TokenData?) -> Void) throws {
        guard let url = URL(string: "/v1/login", relativeTo: baseURL) else {
            throw MeaterServiceError.badURL
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try? JSONEncoder().encode(loginCredentials)
        
//        dump(request)
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if let data = data {
//                let urlContent = NSString(data: data, encoding: String.Encoding.ascii.rawValue)
//                    print(urlContent)
                do {
                    let response = try JSONDecoder().decode(MeaterToken.self, from: data)
//                    print(response as Any)
                    completion(response.data)
                } catch DecodingError.dataCorrupted(let context) {
                    print(context)
                } catch DecodingError.keyNotFound(let key, let context) {
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch DecodingError.valueNotFound(let value, let context) {
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    print("error: ", error)
                }
            }
        }
        task.resume()
    }
}
