//
//  meater-data.swift
//  meater
//
//  Created by Tomas Bulva on 14/10/2023.
//

import Foundation

struct MeaterData: Decodable {
    let data: Data
}

struct Data: Decodable {
    let devices: [Device]
}

struct Device: Decodable, Identifiable {
    let id: String
    let temperature: DeviceTemp
    var cook: Cook?
}

struct DeviceTemp: Decodable {
    let internalTemp: Float?
    let ambientTemp: Float?
    
    enum CodingKeys: String, CodingKey {
        case internalTemp = "internal"
        case ambientTemp = "ambient"
    }
}

struct Cook: Decodable, Identifiable {
    let id: String
    var name: String
    let state: String
    let temperature: CookTemp?
    let time: Time
}

struct CookTemp: Decodable {
    let targetTemp: Float?
    let peakTemp: Float
    
    enum CodingKeys: String, CodingKey {
        case targetTemp = "target"
        case peakTemp = "peak"
    }
}

struct Time: Decodable {
    let elapsed: Int
    let remaining: Int
}

struct MeaterToken: Decodable {
    let data: TokenData
}

struct TokenData: Decodable {
    let token: String
    let userId: String
}

struct LoginCredentials: Encodable {
    let email: String
    let password: String
}

