//
//  meaterApp.swift
//  meater
//
//  Created by Tomas Bulva on 14/10/2023.
//

import SwiftUI

@main
struct meaterApp: App {
    
    private var service = MeaterService(baseURL: URL(string: "https://public-api.cloud.meater.com")!)
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(MeaterModel(meaterService: service)).environmentObject(Auth.shared)
        }
    }
}
