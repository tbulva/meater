//
//  notLoggedIn.swift
//  meater
//
//  Created by Tomas Bulva on 03/11/2023.
//

import SwiftUI

struct notLoggedIn: View {
    var body: some View {
        HStack{
            Spacer()
            VStack {
                Spacer()
                Image("no-chef")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 100)
                Text("You are not logged in")
                    .foregroundColor(Color(nsColor: .controlTextColor))
                Spacer()
            }
            Spacer()
        }
    }
}

#Preview {
    notLoggedIn()
}
