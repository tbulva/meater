//
//  meaterModel.swift
//  meater
//
//  Created by Tomas Bulva on 17/10/2023.
//

import Foundation

@MainActor
class MeaterModel: ObservableObject {
    let meaterService: MeaterService
    
    init(meaterService: MeaterService) {
        self.meaterService = meaterService
    }
    
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var devices: [Device] = []
    
    func login () async throws {
        do {
            try meaterService.getToken(LoginCredentials(
                email: email,
                password: password
            )
            ) { response in
                Auth.shared.setCredentials(
                    accessToken: response!.token
                )
            }
        } catch {
            print("login error: ", error)
        }
    }
    
    func fetchDevices(_ token: String) async {
        print("fetchDevices called")
        do {
            try meaterService.getDevices(token) {
                response in
                DispatchQueue.main.async {
                    if let response = response {
                        self.devices = response
                    }
                }
            }
        } catch {
            print("fetch devices error: ", error)
        }
    }
}
