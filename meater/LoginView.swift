//
//  LoginView.swift
//  meater
//
//  Created by Tomas Bulva on 15/10/2023.
//

import SwiftUI

struct LoginView: View {
    @Environment(\.dismiss) private var dismiss
    @EnvironmentObject private var meaterModel: MeaterModel
    
    var body: some View {
        VStack {
            
            Spacer()
            
            VStack {
                Spacer()
                TextField(
                    "Email",
                    text: $meaterModel.email
                )
                .disableAutocorrection(true)
                .textFieldStyle(.plain)
                
                Spacer()
                
                SecureField(
                    "Password",
                    text: $meaterModel.password
                )
                .textFieldStyle(.plain)
                Spacer()

            }
            
            Spacer()
            HStack {
                Button {
                    dismiss()
                } label: {
                    Text("Cancel")
                        .padding(.horizontal, 8)
                        .frame(maxWidth: .infinity, maxHeight: 40)
                        .foregroundColor(Color(nsColor: .controlTextColor))
                        .background(Color(nsColor: .controlColor))
                        .cornerRadius(8)
                }
                .buttonStyle(.plain)
                Button {
                    Task {
                        try? await meaterModel.login()
                        dismiss()
                    }
                } label: {
                    Text("Login")
                        .padding(.horizontal, 8)
                        .frame(maxWidth: .infinity, maxHeight: 40)
                        .foregroundColor(Color(nsColor: .controlTextColor))
                        .background(Color(nsColor: .controlAccentColor))
                        .cornerRadius(8)
                }
                .buttonStyle(.plain)
                
                
            }
        }
        .padding(30)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        var service = MeaterService(baseURL: URL(string: "https://public-api.cloud.meater.com")!)
        LoginView().environmentObject(MeaterModel(meaterService: service)).environmentObject(Auth.shared)
    }
}
