//
//  NoProbes.swift
//  meater
//
//  Created by Tomas Bulva on 03/11/2023.
//

import SwiftUI

struct NoProbes: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

#Preview {
    NoProbes()
}
