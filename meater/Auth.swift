//
//  Auth.swift
//  meater
//
//  Created by Tomas Bulva on 19/10/2023.
//

import Foundation
import SwiftKeychainWrapper

struct Credentials {
    var accessToken: String?
}

enum KeychainKey: String {
    case accessToken
}

class Auth: ObservableObject {
    
    static let shared: Auth = Auth()
    private let keychain: KeychainWrapper = KeychainWrapper.standard
    
    @Published var loggedIn: Bool = false
    
    private init() {
        DispatchQueue.main.async {
            self.loggedIn = self.hasAccessToken()
        }
    }
    
    func getCredentials() -> Credentials {
        return Credentials(
            accessToken: keychain.string(forKey: KeychainKey.accessToken.rawValue)
        )
    }
    
    func setCredentials(accessToken: String) -> Void {
        keychain.set(accessToken, forKey: KeychainKey.accessToken.rawValue)
        DispatchQueue.main.async {
            self.loggedIn = true
        }
    }
    
    func hasAccessToken() -> Bool {
        return getCredentials().accessToken != nil
    }
    
    func getAccessToken() -> String? {
        return getCredentials().accessToken
    }
    
    func logout() {
        KeychainWrapper.standard.removeObject(forKey: KeychainKey.accessToken.rawValue)
        
        DispatchQueue.main.async {
            self.loggedIn = false
        }
    }
    
}
