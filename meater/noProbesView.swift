//
//  noProbesView.swift
//  meater
//
//  Created by Tomas Bulva on 03/11/2023.
//

import SwiftUI

struct noProbesView: View {
    var body: some View {
        HStack{
            Spacer()
            VStack {
                Spacer()
                Image("no-probe")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 100)
                Text("No probes found.")
                    .foregroundColor(Color(nsColor: .controlTextColor))
                Spacer()
            }
            Spacer()
        }
    }
}

#Preview {
    noProbesView()
}
