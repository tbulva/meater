//
//  meater_peakBundle.swift
//  meater-peak
//
//  Created by Tomas Bulva on 04/11/2023.
//

import WidgetKit
import SwiftUI

@main
struct meater_peakBundle: WidgetBundle {
    var body: some Widget {
        meater_peak()
    }
}
